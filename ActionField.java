package ru.home.morenovaia;

import javax.swing.*;
import java.util.Random;

public class ActionField extends JPanel{
    private Game game;
    private final int WIDTH_FIELD = 6;
    private final int HEIGHT_FIELD = 4;
    private Card cards[];
    private int positions;
    private int countClick;
    private int summ;
    private Timer1Card timer1Card;
    private TimerSomeCard timerSomeCard;
    private TimerDifferentCard timerDifferentCard;

    public ActionField(){
        setLayout(null);
    }

    public void startNewGame(){
        createCards();
        shuffle();
        dropCards();
        countClick = 0;
        summ = 24;
        game.setCount(summ);
    }

    private void createCards(){
        cards = new Card[WIDTH_FIELD * HEIGHT_FIELD];
        int count = 0;
        for (int i = 1; i <= WIDTH_FIELD; i++){
            for (int j = 0; j < HEIGHT_FIELD; j++){
                cards[count] = new Card(i);
                cards[count++].setActionField(this);
            }
        }
    }

    private void shuffle(){
        Random r = new Random();
        for (int i = 0; i < cards.length; i++){
            int index = r.nextInt(cards.length);
            Card a = cards[index];
            cards[index] = cards[i];
            cards[i] = a;
        }
    }

    private void dropCards(){
        for (int i = 0; i < cards.length; i++){
            cards[i].dropCard(i);
        }
    }

    public void choiceCard(int positions){
        countClick++;
        switch (countClick){
            case 1:
                cards[positions].openCard();
                this.positions = positions;
                timer1Card = new Timer1Card();
                timer1Card.setActionField(this);
                timer1Card.setCard(cards[positions]);
                timer1Card.start();
                break;
            case 2:
                timer1Card.stop();
                cards[positions].openCard();
                if (cards[this.positions].equals(cards[positions])){
                    game.setCount(summ -= 2);
                    timerSomeCard = new TimerSomeCard();
                    timerSomeCard.setCard(cards[this.positions], cards[positions]);
                    timerSomeCard.start();
                    resetCountClick();
                } else {
                    timerDifferentCard = new TimerDifferentCard();
                    timerDifferentCard.setActionField(this);
                    timerDifferentCard.setCard(cards[this.positions], cards[positions]);
                    timerDifferentCard.start();
                }
                break;
            case 3:{
                timerDifferentCard.stop();
                timerDifferentCard.action();
                resetCountClick();
                break;
            }
        }
    }

    public void resetCountClick(){
        countClick = 0;
    }

    public void setGame(Game game){
        this.game = game;
    }
}
