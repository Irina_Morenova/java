package ru.home.morenovaia;

public class TimerDifferentCard extends TimerShow{
    public TimerDifferentCard(){
        interval = 3;
    }
    @Override
    protected void action(){
        card1.closeCard();
        card2.closeCard();
        actionField.resetCountClick();
    }
}
