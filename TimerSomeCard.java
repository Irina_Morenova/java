package ru.home.morenovaia;

public class TimerSomeCard extends TimerShow{
    public TimerSomeCard(){
        interval = 2;
    }

    @Override
    protected void action(){
        card1.disableCard();
        card2.disableCard();
    }
}
