package ru.home.morenovaia;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Card{
    private static Pictures pictures;
    private int nameCard;
    private int position;
    private JButton button;
    private JLabel label;
    private ActionField actionField;

    public Card(int nameCard){
        button = new JButton();
        button.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                actionField.choiceCard(position);
            }
        });
        this.nameCard = nameCard;
        label = new JLabel();
        label.setIcon(pictures.getIcon(nameCard));
    }

    public int getNameCard(){
        return nameCard;
    }

    public static void setPictures(Pictures pictures){
        Card.pictures = pictures;
    }

    public void dropCard(int position){
        this.position = position;
        int i = position/6;
        int j = position%6;
        button.setBounds(20 + (100 * j + 10 * j), 10 + (100 * i + 10 * i), 100,100);
        label.setBounds(20 + (100 * j + 10 * j), 10 + (100 * i + 10 * i), 100,100);
        actionField.add(label);
        actionField.add(button);
        closeCard();
    }

    public void openCard(){
        button.setVisible(false);
        label.setVisible(true);
    }

    public void closeCard(){
        label.setVisible(false);
        button.setVisible(true);
    }

    public void disableCard(){
        label.setVisible(false);
    }

    public boolean equals(Card card){
        return card.getNameCard() == nameCard;
    }

    public void setActionField(ActionField actionField){
        this.actionField = actionField;
    }
}
