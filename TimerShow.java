package ru.home.morenovaia;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public abstract class TimerShow implements ActionListener{
    protected ActionField actionField;
    protected Timer timer = new Timer(1000, this);
    protected int interval;
    protected Card card1, card2;

    @Override
    public void actionPerformed(ActionEvent e){
        interval--;
        if (interval == 0){
            timer.stop();
            action();
        }
    }

    protected abstract void action();

    protected void stop(){
        timer.stop();
    }

    protected void start(){
        timer.start();
    }

    public void setCard(Card card1, Card card2){
        this.card1 = card1;
        this.card2 = card2;
    }

    public void setCard(Card card1){
        this.card1 = card1;
    }
    public void setActionField(ActionField actionField){
        this.actionField = actionField;
    }
}
