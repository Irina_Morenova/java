package ru.home.morenovaia;

import javax.swing.*;
import java.awt.*;

public class Form extends JFrame{
    private PlayerDetails playerDetails;
    private Pictures pictures;
    private GameMenu gameMenu;
    private GameSettings gameSettings;
    private TableRecords tableRecords;
    private Game game;

    public Form(){
        setTitle("Memory");
        Image imageIcon = Toolkit.getDefaultToolkit().createImage(getClass().getResource("/ru/home/morenovaia/res/pictures/icon.png"));
        setIconImage(imageIcon);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setState(JFrame.ICONIFIED);
        setLocationRelativeTo(null);
        setSize(700, 530);
        setResizable(false);
        setLayout(new BorderLayout());
    }

    public void init(){
        ButtonTransfer.setForm(this);
        pictures = new Pictures();
        Card.setPictures(pictures);
        gameMenu = new GameMenu();
        gameMenu.setForm(this);
        gameSettings = new GameSettings();
        gameSettings.setForm(this);
        tableRecords = new TableRecords();
        tableRecords.setForm(this);
        gameMenu.setGameSettings(gameSettings);
        gameMenu.setTableRecords(tableRecords);
        gameSettings.setGameMenu(gameMenu);
        tableRecords.setGameMenu(gameMenu);

        gameMenu.init();
        gameSettings.init();
        tableRecords.init();
        add(gameMenu);
    }

    public void initNewGame(){
        game = new Game();
        game.setForm(this);
        game.setGameMenu(gameMenu);
        game.init();
        gameMenu.setVisible(false);
        add(game);
        game.startNewGame();
    }

    public void gameOver(int time){
        if (time > 0){
            playerDetails.checkRecord(time);
        }
        game.setVisible(false);
        tableRecords.setTexts();
        add(tableRecords, BorderLayout.CENTER);
        tableRecords.setVisible(true);
    }

    public void setPlayerDetails(PlayerDetails playerDetails){
        this.playerDetails = playerDetails;
    }

    public PlayerDetails getPlayerDetails(){
        return playerDetails;
    }

    public Pictures getPictures(){
        return pictures;
    }

}

