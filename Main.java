package ru.home.morenovaia;

public class Main{

    public static void main(String[] args){
        PlayerDetails playerDetails = new PlayerDetails();
        playerDetails.openFileName();
        playerDetails.openFile();

        Form form = new Form();
        form.setPlayerDetails(playerDetails);
        form.init();
        form.setVisible(true);
    }
}
