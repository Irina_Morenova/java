package ru.home.morenovaia;

import javax.swing.*;

public class Pictures{
    private int directory = 1;

    public void setDirectory(int directory){
        this.directory = directory;
    }

    public ImageIcon getIcon(int index){
        return new ImageIcon(getClass().getResource("/ru/home/morenovaia/res/pictures/" + directory + "/" + (index) + ".jpg"));
    }
}
