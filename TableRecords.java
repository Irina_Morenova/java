package ru.home.morenovaia;

import javax.swing.*;
import java.awt.*;

public class TableRecords extends JPanel{
    private Form form;
    private GameMenu gameMenu;
    private JPanel table;
    private JLabel[][] line;
    public TableRecords(){
        setLayout(null);
        JLabel headler = new JLabel("Рекорды");
        headler.setHorizontalAlignment(JLabel.CENTER);
        headler.setBounds(250, 30, 200, 50);
        add(headler);

    }

    public void init(){
        back();
        showTable();
        setTexts();
    }

    public void setTexts(){
        line[0][0].setText("Когда");
        line[0][1].setText("Кем установлен");
        line[0][2].setText("Время");

        for (int i = 1; i < line.length; i++){
            for (int j = 0; j < 3; j++){
                line[i][j].setText(form.getPlayerDetails().getText(i-1,j));
            }
        }
    }

    private void showTable(){
        table = new JPanel();
        table.setBounds(100, 80, 500, 244);
        table.setLayout(new GridLayout(6, 3, 1, 1));

        line = new JLabel[6][3];
        for (int i = 0; i < line.length; i++){
            for (int j = 0; j < 3; j++){
                line[i][j] = new JLabel();
                table.add(line[i][j]);
            }
            line[i][2].setHorizontalAlignment(JLabel.RIGHT);
        }
        add(table);
    }

    private void back(){
        ButtonTransfer btnBack = new ButtonTransfer("В меню");
        btnBack.setBounds(250, 364, 200, 50);
        btnBack.setPanelOff(this);
        btnBack.setPanelOn(gameMenu);
        add(btnBack);
    }

    public void showNewRecord(){

    }

    public void setForm(Form form){
        this.form = form;
    }

    public void setGameMenu(GameMenu gameMenu){
        this.gameMenu = gameMenu;
    }
}
