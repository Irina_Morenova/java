package ru.home.morenovaia;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameMenu extends JPanel{
    private String playerName;
    private Form form;
    private GameSettings gameSettings;
    private TableRecords tableRecords;
    private JTextField fielfForPlayerName;


    public GameMenu(){
        setLayout(null);
    }

    public void init(){
        createInputPlayerName();
        createButtons();
    }

    private void createButtons(){
        JButton buttonStartNewGame = new JButton("Новая игра");
        buttonStartNewGame.setBounds(250, 156, 200, 50);
        buttonStartNewGame.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                form.getPlayerDetails().setName(fielfForPlayerName.getText());
                form.initNewGame();
            }
        });

        ButtonTransfer settingsButton = new ButtonTransfer("Настройки");
        settingsButton.setBounds(250, 260, 200, 50);
        settingsButton.setPanelOff(this);
        settingsButton.setPanelOn(gameSettings);

        ButtonTransfer recordsButton = new ButtonTransfer("Рекорды");
        recordsButton.setBounds(250, 364, 200, 50);
        recordsButton.setPanelOff(this);
        recordsButton.setPanelOn(tableRecords);

        add(buttonStartNewGame);
        add(recordsButton);
        add(settingsButton);
    }

    private void createInputPlayerName(){
        fielfForPlayerName = new JTextField(30);
        fielfForPlayerName.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 18));
        fielfForPlayerName.setHorizontalAlignment(JTextField.CENTER);
        fielfForPlayerName.setBounds(250, 52, 200, 50);
        fielfForPlayerName.setText(form.getPlayerDetails().getName());
        add(fielfForPlayerName);
    }

    public String getPlayerName(){
        playerName = fielfForPlayerName.getText();
        return playerName;
    }

    public void setForm(Form form){
        this.form = form;
    }

    public void setGameSettings(GameSettings gameSettings){
        this.gameSettings = gameSettings;
    }

    public void setTableRecords(TableRecords tableRecords){
        this.tableRecords = tableRecords;
    }
}
