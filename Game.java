package ru.home.morenovaia;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Game extends JPanel{
    ActionField actionField;
    private Form form;
    private JPanel infoPanel;
    private boolean pause;
    private TimerGame timerGame;
    private GameMenu gameMenu;
    private JLabel labelCoin;
    private JLabel labelTimer;

    public Game(){
        setLayout(new BorderLayout());
    }

    public void init(){
        infoPanel = new JPanel();
        infoPanel.setLayout(null);
        infoPanel.setPreferredSize(new Dimension(700, 40));

        labelTimer = new JLabel("3:00");
        labelTimer.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 18));
        labelTimer.setBounds(480, 10, 40, 30);
        labelTimer.setForeground(Color.BLUE);
        add(labelTimer);

        labelCoin = new JLabel();
        labelCoin.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 18));
        labelCoin.setBounds(540, 10, 150, 30);
        add(labelCoin);

        JButton btnPause = new JButton("Пауза");
        btnPause.setBounds(130, 10, 320, 30);
        btnPause.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                if (pause){
                    btnPause.setText("Пауза");
                    play();
                } else {
                    btnPause.setText("Продолжить");
                    pause();
                }
                pause = !pause;
            }
        });
        infoPanel.add(btnPause);

        JButton btnBack = new JButton("В меню");
        btnBack.setBounds(20, 10, 100, 30);
            btnBack.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e){
                    timerGame.pauseGame();
                    setVisible(false);
                    form.add(gameMenu);
                    gameMenu.setVisible(true);
                }
            });
        infoPanel.add(btnBack);
        add(infoPanel, BorderLayout.PAGE_START);
    }


    public void startNewGame(){
        pause = false;
        actionField = new ActionField();
        actionField.setGame(this);
        actionField.setPreferredSize(new Dimension(660, 460));
        add(actionField, BorderLayout.CENTER);
        actionField.startNewGame();
        timerGame = new TimerGame();
        timerGame.setGame(this);
        timerGame.playGame();
    }

    public void pause(){
        timerGame.pauseGame();
        actionField.setVisible(false);
    }

    public void play(){
        actionField.setVisible(true);
        timerGame.playGame();
    }

    public void setCount(int summ){
        if (summ == 0){
            timerGame.pauseGame();
            form.gameOver(timerGame.getTime());
        }
        labelCoin.setText("Осталось: " + summ);
    }

    public void setTime(String time){
        labelTimer.setText(time);
    }

    public void setForm(Form form){
        this.form = form;
    }

    public void setGameMenu(GameMenu gameMenu){
        this.gameMenu = gameMenu;
    }

    public void timeWarning(){
        labelTimer.setForeground(Color.RED);
    }
}
