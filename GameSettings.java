package ru.home.morenovaia;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameSettings extends JPanel{
    private static final String[] NAME_DIRECTORY = new String[]{"Птички", "Фрукты", "Смайлики", "Halloween"};
    private Form form;
    private GameMenu gameMenu;
    private JLabel[] labelPreview = new JLabel[6];

    public GameSettings(){
        setLayout(null);
    }

    public void init(){
        back();
        preview();
        picturesPreview();
    }

    private void back(){
        ButtonTransfer btnBack = new ButtonTransfer("В меню");
        btnBack.setBounds(250, 30, 200, 50);
        btnBack.setPanelOff(this);
        btnBack.setPanelOn(gameMenu);
        add(btnBack);
    }

    private void preview(){
        JPanel panelPreview = new JPanel();
        panelPreview.setBounds(50, 200, 600, 100);
        panelPreview.setBackground(Color.WHITE);
        panelPreview.setLayout(new GridLayout());

        for (int i = 0; i < 6; i++){
            labelPreview[i] = new JLabel();
            panelPreview.add(labelPreview[i]);
        }

        JComboBox choicePict = new JComboBox(NAME_DIRECTORY);

        choicePict.setBounds(250, 120, 200, 30);

        choicePict.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                form.getPictures().setDirectory(choicePict.getSelectedIndex() + 1);
                picturesPreview();
                panelPreview.updateUI();
            }
        });

        add(choicePict);
        add(panelPreview);

    }

    private void picturesPreview(){
        for (int i = 0; i < labelPreview.length; i++){
            labelPreview[i].setIcon(form.getPictures().getIcon(i+1));
        }
    }

    public void setGameMenu(GameMenu gameMenu){
        this.gameMenu = gameMenu;
    }

    public void setForm(Form form){
        this.form = form;
    }
}
