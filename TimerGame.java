package ru.home.morenovaia;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class TimerGame implements ActionListener{
    private Game game;
    private Timer timer;
    private int timeRound = 180;

    public TimerGame(){
        timer = new Timer(1000, this);
    }

    @Override
    public void actionPerformed(ActionEvent e){
        timeRound--;
        game.setTime(toString(timeRound));

        if (timeRound == 10){
            game.timeWarning();
        }

        if (timeRound == 0){
            JOptionPane.showMessageDialog(null, "         Время вышло!");
            game.setCount(0);
        }
    }

    public String toString(int time){
        return (time / 60 + (time % 60 < 10 ? ":0" + time % 60 : ":" + time % 60));
    }

    public void pauseGame(){
        timer.stop();
    }

    public void playGame(){
        timer.start();
    }

    public void setGame(Game game){
        this.game = game;
    }

    public int getTime(){
        return timeRound;
    }
}
