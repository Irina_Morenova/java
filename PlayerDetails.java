package ru.home.morenovaia;

import javax.swing.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PlayerDetails{
    private final static String DEFAULT_NAME = "Игрок";
    private final static String[] DEFAULT_RECORDS = new String[]{"12:00_01.01.2018", "_____", "3:00"};
    private String[][] records = new String[5][3];
    private String name = DEFAULT_NAME;

    public void openFile(){
        try (final BufferedReader scn = new BufferedReader(new InputStreamReader(new FileInputStream("D:\\memories\\records.txt"), "UTF-8"))){/*new Scanner(new File("D:\\memories\\records.txt"/*"src//ru.home.morenovaia.res//records.txt")))*/
            readFile(scn);
        } catch (FileNotFoundException fnfe){
            defaultRecords();
            saveRecords();
        } catch (IOException e){
            defaultRecords();
            saveRecords();
        } catch (Exception e){
            defaultRecords();
            saveRecords();
        }
    }

    private void defaultRecords(){
        for (int i = 0; i < 5; i++){
            records[i] = DEFAULT_RECORDS;
        }
    }

    public void readFile(final BufferedReader scn){
        try{
            String line = scn.readLine();
            int row = 0;
            while (line != null){
                records[row] = line.split(" ");
                line = scn.readLine();
                row++;
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public void openFileName(){
        try (final BufferedReader scn = new BufferedReader(new InputStreamReader(new FileInputStream("D:\\memories\\name.txt"), "UTF-8"))){
            readName(scn);
        } catch (FileNotFoundException fnfe){
            defaultRecords();
            saveRecords();
//            fnfe.printStackTrace();
        } catch (IOException e){
            defaultRecords();
            saveRecords();
//            e.printStackTrace();
        } catch (Exception e){
            defaultRecords();
            saveRecords();

        }
        if (checkEmpty(name)){
            name = DEFAULT_NAME;
        }
    }


    private void readName(BufferedReader scn){
        try{
            name = scn.readLine();
        } catch (IOException e){
//            e.printStackTrace();
        }
    }

    public String getName(){
        return name;
    }

    public void setName(String newName){
        if (!name.equals(newName)){
            saveName(newName);
            this.name = newName;
        }
    }

    private void saveName(String newName){
        if (checkEmpty(newName)){
            name = DEFAULT_NAME;
        } else {
            name = newName.replaceAll(" ", "_");
        }

        try (OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("D:\\memories\\name.txt"), StandardCharsets.UTF_8)){
            osw.write(name);
            osw.flush();
        } catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private boolean checkEmpty(String str){
        return (str == null || str.isEmpty());
    }

    public void saveRecords(){
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < 5; i++){
            text.append(records[i][0]).append(" ").append(records[i][1]).append(" ").append(records[i][2]).append(System.getProperty("line.separator"));
        }
        try (OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("D:\\memories\\records.txt"), StandardCharsets.UTF_8)){
            osw.write(text.toString());
            osw.flush();
        } catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public void checkRecord(int time){
        for (int i = 0; i < 5; i++){
            if (180-time < getTime(i)){
                JOptionPane.showMessageDialog(null, "   Установлен новый рекорд!");
                upDateRecords(i, 180-time);
                saveRecords();
                return;
            }
        }
    }

    private void upDateRecords(int line, int time){
        for (int i = 4; i > line; i--){
            records[i][0] = records[i - 1][0];
            records[i][1] = records[i - 1][1];
            records[i][2] = records[i - 1][2];
        }
        records[line][0] = (new SimpleDateFormat("hh:mm'_'dd.MM.yyyy")).format(new Date());
        records[line][1] = name;
        records[line][2] = time / 60 + (time % 60 < 10 ? ":0" + time % 60 : ":" + time % 60);
    }

    public String getText(int i, int j){
        return records[i][j];
    }

    private int getTime(int line){
        return Integer.parseInt(records[line][2].substring(0, 1))*60 + Integer.parseInt(records[line][2].substring(2, 4));
    }
}
