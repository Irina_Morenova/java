package ru.home.morenovaia;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonTransfer extends JButton{
    private static Form form;
    private JPanel panelOff;
    private JPanel panelOn;

    public ButtonTransfer(String text){
        setText(text);
        addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                panelOff.setVisible(false);
                form.add(panelOn);
                panelOn.setVisible(true);
            }
        });
    }

    public static void setForm(Form form){
        ButtonTransfer.form = form;
    }

    public void setPanelOff(JPanel panelOff){
        this.panelOff = panelOff;
    }

    public void setPanelOn(JPanel panelOn){
        this.panelOn = panelOn;
    }
}
